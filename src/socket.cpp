#include "socket.hpp"

using namespace Network;

int Network::sock_create(const bool datagram)
{
	errno = 0;

	int sock;

	if((sock = socket(AF_INET,
		(datagram ? SOCK_DGRAM : SOCK_STREAM), 0)) < 0) {
		throw socket_exception("Failed to create socket!");
	}

	return sock;
}

sock_t Network::sock_open(const int sock, const string host, const port_t port)
{
	errno = 0;

	sockaddr_in addr;
	addr.sin_family = AF_INET;

	const auto he = gethostbyname(host.c_str());
	if(!he) throw socket_exception("Cannot retrieve host's address!");

	const auto a = reinterpret_cast<const char*>(he->h_addr);
	copy(a, a + he->h_length, reinterpret_cast<char*>(&addr.sin_addr.s_addr));

	addr.sin_port = htons(port);

	if(connect(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0) {
		throw socket_exception("Connection refused!");
	}

	return {sock, host};
}

void Network::sock_listen(const int sock, const port_t port)
{
	errno = 0;

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);

	if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0) {
		throw socket_exception("Failed to bind socket!");
	}

	if(listen(sock, SERVER_QUEUE_SIZE) < 0) {
		throw socket_exception("Failed to start listening on socket!");
	}
}

sock_t Network::sock_accept(const int sock)
{
	errno = 0;

	int client_sock;
	sockaddr_in client_addr;
	socklen_t addr_len = sizeof(client_addr);

	if((client_sock = accept(sock,
		reinterpret_cast<sockaddr*>(&client_addr), &addr_len)) < 0) {
		throw socket_exception("Failed to accept incoming connection!");
	}

	return {client_sock, inet_ntoa(client_addr.sin_addr)};
}

void Network::sock_close(const int sock)
{
	close(sock);
}

bool Network::sock_is_ready(const int sock)
{
	errno = 0;

	recv(sock, nullptr, 0, MSG_DONTWAIT | MSG_PEEK);

	if(errno == EWOULDBLOCK) {
		errno = 0;
		return false;
	} else if(errno != 0) {
		throw socket_exception("Failed to check if socket is ready!");
	}

	return true;
}

ssize_t Network::sock_read(const int sock, char* buffer, const size_t size)
{
	errno = 0;

	const auto length = recv(sock, buffer, size, 0);

	if(length < 0 || errno != 0) {
		throw socket_exception("Failed to read from socket!");
	}

	return length;
}

ssize_t Network::sock_read_dgram(const int sock, char* buffer, const size_t size,
	string* host)
{
	errno = 0;

	sockaddr_in addr;
	socklen_t addr_len = sizeof(addr);

	const auto length = recvfrom(sock, buffer, size, 0,
		reinterpret_cast<sockaddr*>(&addr), &addr_len);

	if(errno != 0) {
		throw socket_exception("Failed to read from socket!");
	}

	*host = inet_ntoa(addr.sin_addr);
	return length;
}

void Network::sock_write(const int sock, const char* buffer, const size_t size)
{
	errno = 0;

	send(sock, buffer, size, MSG_NOSIGNAL);

	if(errno != 0) {
		throw socket_exception("Failed to write to socket!");
	}
}

void Network::sock_write_dgram(const int sock, const char* buffer,
	const size_t size, const string host, const port_t port)
{
	errno = 0;

	sockaddr_in addr;
	addr.sin_family = AF_INET;

	const auto he = gethostbyname(host.c_str());
	if(!he) throw socket_exception("Cannot retrieve host's address!");

	const auto a = reinterpret_cast<const char*>(he->h_addr);
	copy(a, a + he->h_length, reinterpret_cast<char*>(&addr.sin_addr.s_addr));

	addr.sin_port = htons(port);

	sendto(sock, buffer, size, MSG_NOSIGNAL,
		reinterpret_cast<sockaddr*>(&addr), sizeof(addr));

	if(errno != 0) {
		throw socket_exception("Failed to write data to the socket!");
	}
}
