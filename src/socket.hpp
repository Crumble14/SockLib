#ifndef SOCKET_HPP
# define SOCKET_HPP

# include<exception>
# include<functional>
# include<iostream>
# include<memory>
# include<sstream>
# include<string>
# include<thread>
# include<unordered_map>
# include<vector>

# include<arpa/inet.h>
# include<errno.h>
# include<fcntl.h>
# include<netdb.h>
# include<netinet/in.h>
# include<stdio.h>
# include<stdlib.h>
# include<string.h>
# include<sys/ioctl.h>
# include<sys/select.h>
# include<sys/socket.h>
# include<sys/types.h>
# include<unistd.h>

# define SERVER_QUEUE_SIZE		16

# define DEFAULT_HTTP_PORT		80
# define DEFAULT_HTTP_TIMEOUT	15000

namespace Network
{
	using namespace std;

	typedef uint16_t port_t;
	typedef uint64_t timestamp_t;
	typedef uint16_t http_code_t;

	class socket_exception : public exception
	{
		public:
			inline socket_exception(const string& message)
				: message{message}
			{}

			inline virtual const char* what() const noexcept
			{
				return message.c_str();
			}

		private:
			const string message;
	};

	typedef struct sock_s
	{
		int fd;
		string host;
	} sock_t;

	int sock_create(const bool datagram);
	sock_t sock_open(const int sock, const string host, const port_t port);
	void sock_listen(const int sock, const port_t port);
	sock_t sock_accept(const int sock);
	void sock_close(const int sock);

	bool sock_is_ready(const int sock);

	ssize_t sock_read(const int sock, char* buffer, const size_t size);
	ssize_t sock_read_dgram(const int sock, char* buffer, const size_t size,
		string* host);
	void sock_write(const int sock, const char* buffer, const size_t size);
	void sock_write_dgram(const int sock, const char* buffer, const size_t size,
		const string host, const port_t port);

	class http_exception : public exception
	{
		public:
			inline http_exception(const string& message)
				: message{message}
			{}

			inline virtual const char* what() const noexcept
			{
				return message.c_str();
			}

		private:
			const string message;
	};

	class HTTPCall
	{
		public:
			inline HTTPCall(const string url)
			{
				set_URL(url);
			}

			inline HTTPCall(const string host, const string path)
				: HTTPCall(host, DEFAULT_HTTP_PORT, path)
			{}

			HTTPCall(const string host, const port_t port, const string path);

			inline const string& get_URL() const
			{
				return url;
			}

			void set_URL(const string url);

			inline const string& get_host() const
			{
				return host;
			}

			inline port_t get_port() const
			{
				return port;
			}

			inline const string& get_path() const
			{
				return path;
			}

			inline const string& get_method() const
			{
				return method;
			}

			inline void set_method(const string method)
			{
				this->method = method;
			}

			inline const unordered_map<string, string>& get_params() const
			{
				return params;
			}

			const string* get_param(string name) const;

			inline void set_param(const string name, const string value)
			{
				params[name] = value;
			}

			inline const string& get_body() const
			{
				return body;
			}

			inline void set_body(const string body)
			{
				this->body = body;
			}

			inline timestamp_t get_timeout() const
			{
				return timeout;
			}

			inline void set_timeout(const timestamp_t timeout)
			{
				this->timeout = timeout;
			}

			string get_request();

			void perform();

			inline http_code_t get_response_code() const
			{
				return response_code;
			}

			inline const unordered_map<string,
				string>& get_response_params() const
			{
				return response_params;
			}

			const string* get_response_param(string name) const;

			inline const string& get_response_body() const
			{
				return response_body;
			}

		private:
			string url;

			string host;
			port_t port = DEFAULT_HTTP_PORT;
			string path;

			string method = "GET";

			unordered_map<string, string> params;
			string body;

			timestamp_t timeout = DEFAULT_HTTP_TIMEOUT;

			http_code_t response_code = 0;
			unordered_map<string, string> response_params;
			string response_body;

			size_t read_header(const int sock, string& buffer);
			void parse_header(const string& buffer, const size_t end);
			void read_body(const int sock,
				string& buffer, const size_t header_end);
	};
}

#endif
