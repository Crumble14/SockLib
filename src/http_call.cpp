#include "socket.hpp"

using namespace Network;

/*static vector<string> str_split(const string& str, const char&& separator)
{
	stringstream ss(str);
	string buffer;

	vector<string> result;
	auto r = back_inserter(result);

	while(getline(ss, buffer, separator)) {
		*(r++) = buffer;
	}

	return result;
}

static inline timestamp_t get_timestamp()
{
	using namespace std::chrono;
	return time_point_cast<milliseconds>(system_clock::now())
		.time_since_epoch().count();
}

static void to_lower_case(string& str)
{
	for(size_t i = 0; i < str.size(); ++i) {
		auto& c = str[i];

		if(c >= 'A' && c <= 'Z') {
			c += 'a' - 'A';
		}
	}
}

static void trim(string& str)
{
	while(!str.empty()) {
		if(str.back() > ' ' && str.back() != 127) break;
		str.pop_back();
	}

	while(!str.empty()) {
		if(str.front() > ' ' && str.front() != 127) break;
		str.erase(0, 1);
	}
}

HTTPCall::HTTPCall(const string host, const port_t port, const string path)
	: host{host}, port{port}, path{path}
{
	if(port != DEFAULT_HTTP_PORT) {
		this->url = "http://" + host + ':' + to_string(port) + '/' + path;
	} else {
		this->url = "http://" + host + '/' + path;
	}
}

void HTTPCall::set_URL(const string url)
{
	// TODO Non-ASCII characters support
	// TODO HTTPS support
	if(url.find("https://") == 0) {
		throw http_exception("HTTPS not supported!");
	}

	if(url.find("http://") != 0) {
		throw http_exception("URL syntax error!");
	}

	string buffer(url.cbegin() + 7, url.cend());
	const auto split = str_split(buffer, '/');
	if(split.empty()) throw http_exception("Invalid URL!");

	host = split.front();

	if(split.size() > 1) {
		buffer.erase(0, host.size());
		path = buffer;
	}

	if(path.empty()) path = '/';

	size_t pos;

	if((pos = host.find(':')) != string::npos) {
		if(host.find(':', pos + 1) != string::npos) {
			throw http_exception("URL syntax error!");
		}

		const auto b(host);
		host = string(b.cbegin(), b.cbegin() + pos);
		port = atoi(b.c_str() + pos + 1);
	}

	this->url = url;
}

const string* HTTPCall::get_param(string name) const
{
	to_lower_case(name);

	for(const auto& p : params) {
		auto n = p.first;
		to_lower_case(n);

		if(n == name) return &p.second;
	}

	return nullptr;
}

string HTTPCall::get_request()
{
	string request = method + ' ' + path + " HTTP/1.1\r\n";

	set_param("Host", (port != 80 ? host + ':' + to_string(port) : host));
	if(!body.empty()) set_param("Content-Length", to_string(body.size()));

	for(const auto& p : params) {
		request += p.first + ": " + p.second + "\r\n";
	}

	request += "\r\n";
	if(!body.empty()) request += body;

	return request;
}

void HTTPCall::perform()
{
	const auto request = get_request();

	const auto sock = sock_create(false);
	sock_open(sock, host, port);

	sock_write(sock, request.data(), request.size());

	string buffer;
	const auto header_end = read_header(sock, buffer);
	parse_header(buffer, header_end);
	read_body(sock, buffer, header_end);
}

size_t HTTPCall::read_header(const int sock, string& buffer)
{
	size_t i = 0;
	size_t header_end;

	const auto begin = get_timestamp();

	// Currently O(n^2)
	// TODO Find a way to get the amount of pending bytes
	while(true) {
		if(!sock_is_ready(sock)) {
			if(get_timestamp() < (begin + DEFAULT_HTTP_TIMEOUT)) {
				continue;
			} else {
				throw http_exception("Request timed out!");
			}
		}

		char* b = (char*) malloc(1024);
		const auto length = sock_read(sock, b, 1024);
		buffer += b;
		free((void*) b);

		if((header_end = buffer.find("\r\n\r\n",
			(i < 4 ? 0 : i - 4))) != string::npos) {
			break;
		}

		i += length;
	}

	return header_end + 4;
}

void HTTPCall::parse_header(const string& buffer, const size_t end)
{
	stringstream response_stream(string(buffer.cbegin(),
		buffer.cbegin() + end));
	string line;
	bool first = true;

	while(getline(response_stream, line)) {
		trim(line);
		if(line.empty()) continue;

		if(first) {
			const auto split = str_split(line, ' ');

			if(split.size() < 3) {
				throw http_exception("Bad HTTP response!");
			}

			response_code = stoi(split[1]);

			first = false;
			continue;
		}

		string name, value;
		size_t pos = 0;

		if((pos = line.find(':')) == string::npos) {
			name = string(line.cbegin(), line.cend());
		} else {
			name = string(line.cbegin(), line.cbegin() + pos);
			value = string(line.cbegin() + pos + 1, line.cend());
		}

		trim(name);
		trim(value);
		response_params.emplace(name, value);
	}
}

void HTTPCall::read_body(const int sock,
	string& buffer, const size_t header_end)
{
	(void) socket;
	(void) buffer;
	(void) header_end;
	// TODO
	const auto content_length = get_response_param("Content-Length");
	size_t length;

	if(content_length) {
		length = stoi(*content_length) - (buffer.size() - header_end) + 2; // TODO Find why `+ 2` is needed in order to read the entire body
		
		// TODO Read 'length' characters (and handle timeouts)
	} else {
		// TODO Read with a small timeout until the socket isn't ready anymore (maybe there could be a much better way)
	}

	response_body = string(buffer.cbegin() + header_end, buffer.cend());
}

const string* HTTPCall::get_response_param(string name) const
{
	to_lower_case(name);

	for(const auto& p : response_params) {
		auto n = p.first;
		to_lower_case(n);

		if(n == name) return &p.second;
	}

	return nullptr;
}*/
